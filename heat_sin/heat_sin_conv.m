clear

% variables declaration
t = 2; a = 1; N = 100;
int_a = -2*pi; int_b = 2*pi;
arr_len = 1001;
x = linspace(int_a, int_b, arr_len);
norm_1 = zeros(1, N);
norm_2 = zeros(1, N);
norm_3 = zeros(1, N);

% auxiliary functions
y = linspace(1, N+1, arr_len);
ln_1 = @(x)(-x);
ln_2 = @(x)(-2*x);
inv = @(x)(1./x);
inv_2 = @(x)(1./(x.^2));

% initial condition and solution
init = @(x)(sin(x));
solution = @(x)(exp(-a*t)*sin(x));

% iteration on composition degree
for n = 1:N
    
    % shift components
    tau_R = 2*a*sqrt(t/n);
    tau_V = a*sqrt(6*t/n);    
    
    % computing Chernoff functions
    for p = 0:n
        
        % declaring coefficients
        alpha_R = 0;
        alpha_V = 0;
        
        % computing coefficients
        for k = 0:floor((n - p)/2)
            alpha_R = alpha_R + nchoosek(n, k)*nchoosek(n-k, k+p)*2^(n-p-2*k);
            alpha_V = alpha_V + nchoosek(n, k)*nchoosek(n-k, k+p)*4^(n-p-2*k);
        end
        
        % computing Chernoff
        if (p == 0)
            chern_R = alpha_R*init(x);
            chern_V = alpha_V*init(x);
        else
            chern_R = chern_R + alpha_R*(init(x + p*tau_R) + init(x - p*tau_R));
            chern_V = chern_V + alpha_V*(init(x + p*tau_V) + init(x - p*tau_V));
        end
    end
    
    % computing norms
    
    % Remizov function
    chern_R = 4^(-n)*chern_R;
    norm_1(n) = max(abs(chern_R - solution(x)));

    % Vedenin's function
    chern_V = 3^(-n)*(2 + cos(a*sqrt(6*t/n)))^(n)*init(x);
    norm_2(n) = max(abs(chern_V - solution(x)));
    
    % Fast converging function
    chern_F = 15^(-n)*(5 + cos(a*sqrt(12*t/n)) + 9*cos(a*sqrt(2*t/n)))^(n)*init(x);
    norm_3(n) = max(abs(chern_F - solution(x)));
    
    if (n == 2)
        chern_iter_1 = chern_R;
        chern_iter_2 = chern_V;
        chern_iter_3 = chern_F;
    end
end

% plotting graphs
figure
hold on

% axes names and limits
xlabel('n', 'FontSize', 16);
xlim([4.5 N + 1]);
y_lim = 0.02;
ylim([-0.001 y_lim]);

% title
textx = (N + 5.5)/2;
text(textx, y_lim - 0.00125, ...
    ['Convergence speed of Chernoff approximations ', ...
    'to the solution of the heat equation $u^\prime_t = a\cdot u^{\prime\prime}_{xx}$'], ...
    'Interpreter', 'latex', 'FontSize', 24, ...
    'HorizontalAlignment', 'center');
text(textx, y_lim - 0.00275, ...
    ['for ', ...
    '$u_0(x) = \sin(x)$, $a = 1$, and $t = 2$'], ...
    'Interpreter', 'latex', 'FontSize', 24, ...
    'HorizontalAlignment', 'center');

% 1st approximation
plot(1:N, norm_1, 'o', 'MarkerSize', 8, ...
    'MarkerEdgeColor', 'b', 'MarkerFaceColor', 'b');
textx_1 = 25;
textx = textx_1 + 5;
texty_1 = norm_1(textx_1);
texty = texty_1 + 0.004;
plot([textx_1, textx], [texty_1, texty], 'b--', 'LineWidth', 1.5, ...
    'HandleVisibility', 'off');
text(textx, texty, ...
    '$\,$Norm decay for Chernoff approximation via (16)', ...
    'FontSize', 22, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% 2nd approximation
plot(1:N, norm_2, 'o', 'MarkerSize', 8, ...
    'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r');
textx_1 = 14;
textx = textx_1 + 12;
texty_1 = norm_2(textx_1);
texty = texty_1 + 0.00975;
plot([textx_1, textx], [texty_1, texty], 'r--', 'LineWidth', 1.5, ...
    'HandleVisibility', 'off');
text(textx, texty, ...
    '$\,$Norm decay for Chernoff approximation via (17)', ...
    'FontSize', 22, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% 3rd approximation
plot(1:N, norm_3, 'o', 'MarkerSize', 8, ...
    'MarkerEdgeColor', '#77AC30', 'MarkerFaceColor', '#77AC30');
textx_1 = 7;
textx = textx_1 + 15;
texty_1 = norm_3(textx_1);
texty = texty_1 + 0.013;
plot([textx_1, textx], [texty_1, texty], 'LineStyle', '--', ...
    'Color', '#77AC30', 'LineWidth', 1.5, 'HandleVisibility', 'off');
text(textx, texty, ...
    '$\,$Norm decay for Chernoff approximation via (18)', ...
    'FontSize', 22, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

hold off