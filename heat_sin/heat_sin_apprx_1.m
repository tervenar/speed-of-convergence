clear

% variables declaration
t = 2; a = 1;
int_a = -2*pi; int_b = 2*pi;
arr_len = 1001;
x = linspace(int_a, int_b, arr_len);
N = 100;
norm_1 = zeros(1, N);
norm_2 = zeros(1, N);
norm_3 = zeros(1, N);

% auxiliary functions
y = linspace(1, N+1, arr_len);
ln_1 = @(x)(-x);
ln_2 = @(x)(-2*x);
inv = @(x)(1./x);
inv_2 = @(x)(1./(x.^2));

% initial condition and solution
init = @(x)(sin(x));
solution = @(x)(exp(-a*t)*sin(x));

% iteration on composition degree
for n = 1:N
    
    % shift components
    tau_R = 2*a*sqrt(t/n);
    tau_V = a*sqrt(6*t/n);    
    
    % computing Chernoff functions
    for p = 0:n
        
        % declaring coefficients
        alpha_R = 0;
        alpha_V = 0;
        
        % computing coefficients
        for k = 0:floor((n - p)/2)
            alpha_R = alpha_R + nchoosek(n, k)*nchoosek(n-k, k+p)*2^(n-p-2*k);
            alpha_V = alpha_V + nchoosek(n, k)*nchoosek(n-k, k+p)*4^(n-p-2*k);
        end
        
        % computing Chernoff
        if (p == 0)
            chern_R = alpha_R*init(x);
            chern_V = alpha_V*init(x);
        else
            chern_R = chern_R + alpha_R*(init(x + p*tau_R) + init(x - p*tau_R));
            chern_V = chern_V + alpha_V*(init(x + p*tau_V) + init(x - p*tau_V));
        end
    end
    
    % computing norms
    
    % Remizov function
    chern_R = 4^(-n)*chern_R;
    norm_1(n) = max(abs(chern_R - solution(x)));

    % Vedenin's function
    chern_V = 3^(-n)*(2 + cos(a*sqrt(6*t/n)))^(n)*init(x);
    norm_2(n) = max(abs(chern_V - solution(x)));
    
    % Fast converging function
    chern_F = 15^(-n)*(5 + cos(a*sqrt(12*t/n)) + 9*cos(a*sqrt(2*t/n)))^(n)*init(x);
    norm_3(n) = max(abs(chern_F - solution(x)));
    
    if (n == 1)
        chern_iter_1 = chern_R;
        chern_iter_2 = chern_V;
        chern_iter_3 = chern_F;
        break
    end
end

% plotting graphs
figure
hold on

% axes names and limits
xlabel('x', 'FontSize', 18);
ylabel('u', 'FontSize', 18);
ax = gca;
set(gca, 'FontSize', 18);
ax.XAxisLocation = 'origin';
% ax.YAxisLocation = 'origin';
xlim([int_a int_b]);
y_lim = 0.65;
ylim([-0.4 0.5]);

% title
textx = (int_b + int_a)/2;
% text(textx, y_lim - 0.075, ...
%     ['Approximations to the solution of the heat equation ', ...
%     '$u^\prime_t = a\cdot u^{\prime\prime}_{xx}$ ', ...
%     'for $u_0(x) = \sin(x)$, '], ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');
% text(textx, y_lim - 0.15, ...
%     '$a = 1$, and $t = 2$, using Chernoff functions (n = 1)', ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');
        
% solution
plot(x, solution(x), '-k', 'LineWidth', 1.75); 
textx_1 = -1.5;
texty_1 = solution(textx_1);
textx = textx_1 + 0.7;
texty = texty_1 - 0.175;
text(textx, texty, '$\,$Exact solution $u(t, x)$ given by (20)', ...
    'FontSize', 26, 'FontWeight', 'Bold', 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');
plot([textx_1, textx], [texty_1, texty], ...
    'k--', 'LineWidth', 1.75);
        
% 1st approximation
plot(x, chern_iter_1, '-b', 'LineWidth', 1.75);
textx_1 = -4.75;
textx = textx_1 + 1;
texty_1 = chern_iter_1(floor((textx_1 - int_a)*(arr_len + 1)/(int_b-int_a)));
texty = texty_1 + 0.25;
plot([textx_1, textx], [texty_1, texty], 'b--', 'LineWidth', 1.75);
text(textx, texty, ...
    '$\,$Chernoff approximation via (15)', ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% 2nd approximation
plot(x, chern_iter_2, '-r', 'LineWidth', 1.75)
textx_1 = -4.25;
textx = textx_1 + 0.45;
texty_1 = chern_iter_2(floor((textx_1 - int_a)*(arr_len + 1)/(int_b-int_a)));
texty = texty_1 + 0.095;
plot([textx_1, textx], [texty_1, texty], 'r--', 'LineWidth', 1.75);
text(textx, texty, ...
    '$\,$Chernoff approximation via (16)', ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% 3rd approximation
plot(x, chern_iter_3, 'LineStyle', '-', 'Color', '#77AC30', 'LineWidth', 1.75)
textx_1 = -1.15;
textx = textx_1 + 0.65;
texty_1 = chern_iter_3(floor((textx_1 - int_a)*(arr_len + 1)/(int_b-int_a)));
texty = texty_1 - 0.15;
plot([textx_1, textx], [texty_1, texty], 'LineStyle', '--', ...
    'Color', '#77AC30', 'LineWidth', 1.75);
text(textx, texty, ...
    '$\,$Chernoff approximation via (17)', ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

hold off