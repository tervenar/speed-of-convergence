clear

% variables declaration
t = 2; a = 1; N = 100;
int_a = -2*pi; int_b = 8;
arr_len = 1001;
x = linspace(int_a, int_b, arr_len);
n_lin = linspace(6, N, arr_len);
norm_1 = zeros(1, N);
norm_2 = zeros(1, N);
norm_3 = zeros(1, N);

% auxiliary functions
y = linspace(1, N+1, arr_len);
ln_1 = @(x)(-x);
ln_2 = @(x)(-2*x);
inv = @(x)(1./x);
inv_2 = @(x)(1./(x.^2));

% initial condition and solution
init = @(x)(sin(x));
solution = @(x)(exp(-a*t)*sin(x));

% iteration on composition degree
for n = 1:N
    
    % shift components
    tau_R = 2*a*sqrt(t/n);
    tau_V = a*sqrt(6*t/n);    
    
    % computing Chernoff functions
    for p = 0:n
        
        % declaring coefficients
        alpha_R = 0;
        alpha_V = 0;
        
        % computing coefficients
        for k = 0:floor((n - p)/2)
            alpha_R = alpha_R + nchoosek(n, k)*nchoosek(n-k, k+p)*2^(n-p-2*k);
            alpha_V = alpha_V + nchoosek(n, k)*nchoosek(n-k, k+p)*4^(n-p-2*k);
        end
        
        % computing Chernoff
        if (p == 0)
            chern_R = alpha_R*init(x);
            chern_V = alpha_V*init(x);
        else
            chern_R = chern_R + alpha_R*(init(x + p*tau_R) + init(x - p*tau_R));
            chern_V = chern_V + alpha_V*(init(x + p*tau_V) + init(x - p*tau_V));
        end
    end
    
    % computing norms
    
    % Remizov function
    chern_R = 4^(-n)*chern_R;
    norm_1(n) = max(abs(chern_R - solution(x)));

    % Vedenin's function
    chern_V = 3^(-n)*(2 + cos(a*sqrt(6*t/n)))^(n)*init(x);
    norm_2(n) = max(abs(chern_V - solution(x)));
    
    % Fast converging function
    chern_F = 15^(-n)*(5 + cos(a*sqrt(12*t/n)) + 9*cos(a*sqrt(2*t/n)))^(n)*init(x);
    norm_3(n) = max(abs(chern_F - solution(x)));
    
    if (n == 2)
        chern_iter_1 = chern_R;
        chern_iter_2 = chern_V;
        chern_iter_3 = chern_F;
    end
end

% computing linear regression
regr_beta_1 = fitlm(log(6:N), log(norm_1(6:N)), 'linear').Coefficients.Estimate(1);
regr_alpha_1 = fitlm(log(6:N), log(norm_1(6:N)), 'linear').Coefficients.Estimate(2);
regr_beta_2 = fitlm(log(6:N), log(norm_2(6:N)), 'linear').Coefficients.Estimate(1);
regr_alpha_2 = fitlm(log(6:N), log(norm_2(6:N)), 'linear').Coefficients.Estimate(2);
regr_beta_3 = fitlm(log(6:N), log(norm_3(6:N)), 'linear').Coefficients.Estimate(1);
regr_alpha_3 = fitlm(log(6:N), log(norm_3(6:N)), 'linear').Coefficients.Estimate(2);

regr_1 = exp(regr_beta_1).*n_lin.^regr_alpha_1;
regr_2 = exp(regr_beta_2).*n_lin.^regr_alpha_2;
regr_3 = exp(regr_beta_3).*n_lin.^regr_alpha_3;

% plotting graphs
figure
hold on

% axes names and limits
xlabel('n', 'FontSize', 18);
ylabel('u', 'FontSize', 18);
ax = gca;
set(gca, 'FontSize', 18);
ax.XAxisLocation = 'origin';
% ax.YAxisLocation = 'origin';
xlim([4.5 N + 1]);
y_lim = 0.02;
ylim([0 0.016]);

% title
textx = (N + 3.5)/2;
% text(textx, y_lim - 0.00125, ...
%     'Estimates of the convergence speed of Chernoff approximations to the solution', ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');
% text(textx, y_lim - 0.00275, ...
%     ['of the heat equation $u^\prime_t = a\cdot u^{\prime\prime}_{xx}$',...
%     ' for $u_0(x) = \sin(x)$, $a = 1$, and $t = 2$'], ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');

% 1st approximation
plot(6:N, norm_1(6:N), 'o', 'MarkerSize', 8, ...
    'MarkerEdgeColor', 'b', 'MarkerFaceColor', 'b');
plot(n_lin, regr_1, 'k-', 'LineWidth', 1.75, 'HandleVisibility', 'off');
textx_1 = 25;
textx = textx_1 + 5;
texty_1 = regr_1(floor((textx_1 - 6)*(arr_len + 1)/(N-6)));
texty = texty_1 + 0.004;
plot([textx_1, textx], [texty_1, texty], 'k--', 'LineWidth', 1.75, ...
    'HandleVisibility', 'off');
text(textx, texty, ...
    ['$\, \beta_1\left(\frac{1}{n}\right)^{\alpha_1}$ with $\beta_1 = ', ...
    num2str(regr_beta_1, '%3.4f'), '$, $\alpha_1 = ', ...
    num2str(regr_alpha_1, '%3.4f'), '$'], ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% 2nd approximation
plot(6:N, norm_2(6:N), 'o', 'MarkerSize', 8, ...
    'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r');
plot(n_lin, regr_2, 'k-', 'LineWidth', 1.75, 'HandleVisibility', 'off');
textx_1 = 14;
textx = textx_1 + 12;
texty_1 = regr_2(floor((textx_1 - 6)*(arr_len + 1)/(N-6)));
texty = texty_1 + 0.00975;
plot([textx_1, textx], [texty_1, texty], 'k--', 'LineWidth', 1.75, ...
    'HandleVisibility', 'off');
text(textx, texty, ...
    ['$\, \beta_2\left(\frac{1}{n}\right)^{\alpha_2}$ with $\beta_2 = ', ...
    num2str(regr_beta_2, '%3.4f'), '$, $\alpha_2 = ', ...
    num2str(regr_alpha_2, '%3.4f'), '$'], ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% 3rd approximation
plot(6:N, norm_3(6:N), 'o', 'MarkerSize', 8, ...
    'MarkerEdgeColor', '#77AC30', 'MarkerFaceColor', '#77AC30');
plot(n_lin, regr_3, 'k-', 'LineWidth', 1.75, 'HandleVisibility', 'off');
textx_1 = 7;
textx = textx_1 + 15;
texty_1 = regr_3(floor((textx_1 - 6)*(arr_len + 1)/(N-6)));
texty = texty_1 + 0.013;
plot([textx_1, textx], [texty_1, texty], '--', 'Color', 'k', ...
    'LineWidth', 1.75, 'HandleVisibility', 'off');
text(textx, texty, ...
    ['$\, \beta_3\left(\frac{1}{n}\right)^{\alpha_3}$ with $\beta_3 = ', ...
    num2str(regr_beta_3, '%3.4f'), '$, $\alpha_3 = ', ...
    num2str(regr_alpha_3, '%3.4f'), '$'], ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% legend
lgd = legend('Norm decay for Chernoff approximation via (15)', ...
    'Norm decay for Chernoff approximation via (16)', ...
    'Norm decay for Chernoff approximation via (17)', ...
    'Interpreter', 'latex', 'FontSize', 26);
rect = [0.55, 0.21, .25, .25];
set(lgd, 'Position', rect)

hold off