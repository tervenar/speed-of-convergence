clear

% variables declaration
t = 1; a = 1; N = 100;
int_a = -50; int_b = 50;
arr_len = 5001;
x = linspace(int_a, int_b, arr_len);
n_lin = linspace(1, N, arr_len);
norm_1 = zeros(1, N);
norm_2 = zeros(1, N);
norm_3 = zeros(1, N);

% auxiliary functions
y = linspace(1, N+1, arr_len);
ln_1 = @(x)(-x);
ln_2 = @(x)(-2*x);
inv = @(x)(1./x);
inv_2 = @(x)(1./(x.^2));

% initial condition and solution
init = @(x)(exp(-abs(x)));
solution = @(x)(exp(t-x).*(1 - 1/2*erfc(x/(2*sqrt(t)) - sqrt(t))) + ...
       exp(t+x)*1/2.*erfc(x/(2*sqrt(t)) + sqrt(t)));

% iteration on composition degree
for n = 1:N
    
    % shift components
    tau_R = 2*a*sqrt(t/n);
    tau_V = a*sqrt(6*t/n);    
    
    % computing Chernoff functions
    for p = 0:n
        
        % declaring coefficients
        alpha_R = 0;
        alpha_V = 0;
        
        % computing coefficients
        for k = 0:floor((n - abs(p))/2)
            alpha_R = alpha_R + nchoosek(n, k)*nchoosek(n-k, k + abs(p))*2^(n - abs(p) - 2*k);
            alpha_V = alpha_V + nchoosek(n, k)*nchoosek(n-k, k + abs(p))*4^(n - abs(p) - 2*k);
        end
        
        % computing Chernoff
        if (p == 0)
            chern_R = alpha_R*init(x);
            chern_V = alpha_V*init(x);
        else
            chern_R = chern_R + alpha_R*(init(x + p*tau_R) + init(x - p*tau_R));
            chern_V = chern_V + alpha_V*(init(x + p*tau_V) + init(x - p*tau_V));
        end
    end
    
    % computing norms
    
    % Remizov function
    chern_R = 4^(-n)*chern_R;
    norm_1(n) = max(abs(chern_R - solution(x)));

    % Vedenin's function
    chern_V = 6^(-n)*chern_V;
    norm_2(n) = max(abs(chern_V - solution(x)));
end

% computing linear regression
regr_beta_1 = fitlm(log(6:N), log(norm_1(6:N)), 'linear').Coefficients.Estimate(1);
regr_alpha_1 = fitlm(log(6:N), log(norm_1(6:N)), 'linear').Coefficients.Estimate(2);
regr_beta_2 = fitlm(log(6:N), log(norm_2(6:N)), 'linear').Coefficients.Estimate(1);
regr_alpha_2 = fitlm(log(6:N), log(norm_2(6:N)), 'linear').Coefficients.Estimate(2);

regr_1 = exp(regr_beta_1).*n_lin.^regr_alpha_1;
regr_2 = exp(regr_beta_2).*n_lin.^regr_alpha_2;

% plotting graphs
figure
hold on
set(gca, 'XAxisLocation', 'top')

% axes names and limits
xlabel('log(n)', 'FontSize', 18);
ylabel('u', 'FontSize', 18);
ax = gca;
set(gca, 'FontSize', 18);
ax.XAxisLocation = 'origin';
% ax.YAxisLocation = 'origin';
xlim([0 4.61]);
y_lim = 0;
ylim([-6.25 -1]);

% title
textx = (4.6)/2;
% text(textx, y_lim - 0.35, ...
%     'Estimates of the convergence speed of Chernoff approximations to the solution', ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');
% text(textx, y_lim - 0.7, ...
%     ['of the heat equation $u^{\prime}_t = a\cdot u^{\prime\prime}_{xx}$',...
%     ' for $u_0(x) = \exp(-|x|)$, $a = 1$, and $t = 1$'], ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');
% text(textx, y_lim - 1.05, ...
%     '(log-log scale)', ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');


% 1st approximation
plot(log(1:N), log(norm_1(1:N)), 'o', 'MarkerEdgeColor', 'b', ...
    'MarkerFaceColor', 'b', 'MarkerSize', 8);
plot(log(n_lin), log(regr_1), 'k-', 'LineWidth', 1.75, 'HandleVisibility', 'off');
textx_1 = 2.25;
textx = textx_1 - 0.2;
texty_1 = log(regr_1(floor((exp(textx_1) - 1)*(arr_len + 1)/(N-1))));
texty = texty_1 - 1;
plot([textx_1, textx], [texty_1, texty], 'k--', 'LineWidth', 1.75, 'HandleVisibility', 'off');
text(textx, texty, ...
    ['$\, \beta_1\left(\frac{1}{n}\right)^{\alpha_1}$ with $\beta_1 = ', ...
    num2str(exp(regr_beta_1), '%3.3f'), '$, $\alpha_1 = ', ...
    num2str(regr_alpha_1, '%3.3f'), '$'], ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'right', 'Interpreter', 'latex');

% 2nd approximation
plot(log(1:N), log(norm_2(1:N)), 'o', 'MarkerSize', 8, ...
    'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r');
plot(log(n_lin), log(regr_2), 'k-', 'LineWidth', 1.75, 'HandleVisibility', 'off');
textx_1 = 1.7;
textx = textx_1 + 0.2;
texty_1 = log(regr_2(floor((exp(textx_1) - 1)*(arr_len + 1)/(N-1))));
texty = texty_1 + 1;
plot([textx_1, textx], [texty_1, texty], 'k--', 'LineWidth', 1.75, 'HandleVisibility', 'off');
text(textx, texty, ...
    ['$\, \beta_2\left(\frac{1}{n}\right)^{\alpha_2}$ with $\beta_2 = ', ...
    num2str(exp(regr_beta_2), '%3.3f'), '$, $\alpha_2 = ', ...
    num2str(regr_alpha_2, '%3.3f'), '$'], ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

legend('Norm decay for Chernoff approximation via (15)', ...
    'Norm decay for Chernoff approximation via (16)', ...
    'Location', 'southwest', 'Interpreter', 'latex', 'FontSize', 26);

%icons = findobj(icons, 'Marker', 'none', '-xor');
%set(icons, 'MarkerSize', 100);

hold off