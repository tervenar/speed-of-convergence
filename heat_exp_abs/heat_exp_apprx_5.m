clear

% variables declaration
t = 1; a = 1; N = 100;
int_a = -4; int_b = 4;
arr_len = 1001;
x = linspace(int_a, int_b, arr_len);
norm_1 = zeros(1, N);
norm_2 = zeros(1, N);
norm_3 = zeros(1, N);

% auxiliary functions
y = linspace(1, N+1, arr_len);
ln_1 = @(x)(-x);
ln_2 = @(x)(-2*x);
inv = @(x)(1./x);
inv_2 = @(x)(1./(x.^2));

% initial condition and solution
init = @(x)(exp(-abs(x)));
solution = @(x)(exp(t-x).*(1 - 1/2*erfc(x/(2*sqrt(t)) - sqrt(t))) + ...
       exp(t+x)*1/2.*erfc(x/(2*sqrt(t)) + sqrt(t)));

% iteration on composition degree
for n = 1:N
    
    % shift components
    tau_R = 2*a*sqrt(t/n);
    tau_V = a*sqrt(6*t/n);    
    
    % computing Chernoff functions
    for p = 0:n
        
        % declaring coefficients
        alpha_R = 0;
        alpha_V = 0;
        
        % computing coefficients
        for k = 0:floor((n - abs(p))/2)
            alpha_R = alpha_R + nchoosek(n, k)*nchoosek(n-k, k + abs(p))*2^(n - abs(p) - 2*k);
            alpha_V = alpha_V + nchoosek(n, k)*nchoosek(n-k, k + abs(p))*4^(n - abs(p) - 2*k);
        end
        
        % computing Chernoff
        if (p == 0)
            chern_R = alpha_R*init(x);
            chern_V = alpha_V*init(x);
        else
            chern_R = chern_R + alpha_R*(init(x + p*tau_R) + init(x - p*tau_R));
            chern_V = chern_V + alpha_V*(init(x + p*tau_V) + init(x - p*tau_V));
        end
    end
    
    % computing norms
    
    % Remizov function
    chern_R = 4^(-n)*chern_R;
    norm_R(n) = max(abs(chern_R - solution(x)));

    % Vedenin's function
    chern_V = 6^(-n)*chern_V;
    norm_V(n) = max(abs(chern_V - solution(x)));
    
    if (n == 5)
        chern_iter_1 = chern_R;
        chern_iter_2 = chern_V;
        break
    end
end

% plotting graphs
figure
hold on

% axes names and limits
xlabel('x', 'FontSize', 18);
ylabel('u', 'FontSize', 18);
ax = gca;
set(gca, 'FontSize', 18);
ax.XAxisLocation = 'origin';
% ax.YAxisLocation = 'origin';
xlim([int_a int_b]);
y_lim = 0.6;
ylim([0 y_lim]);

% title
textx = (int_b + int_a)/2;
% text(textx, y_lim - 0.04, ...
%     ['Approximations to the solution of the heat equation ', ...
%     '$u^{\prime}_t = a\cdot u^{\prime\prime}_{xx}$ ', ...
%     'for $u_0(x) = \exp(-|x|)$, '], ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');
% text(textx, y_lim - 0.0815, ...
%     '$a = 1$, and $t = 1$, using Chernoff functions (n = 5)', ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');
        
% solution
plot(x, solution(x), '-k', 'LineWidth', 1.75); 
textx_1 = 0.32;
texty_1 = solution(textx_1);
textx = textx_1 + 0.75;
texty = texty_1 + 0.075;
text(textx, texty, '$\,$Exact solution $u(t, x)$ given by (24)', ...
    'FontSize', 26, 'FontWeight', 'Bold', 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');
plot([textx_1, textx], [texty_1, texty], ...
    'k--', 'LineWidth', 1.75);
        
% 1st approximation
plot(x, chern_iter_1, '-b', 'LineWidth', 1.75);
textx_1 = -0.82;
textx = textx_1 - 0.45;
texty_1 = chern_iter_1(floor((textx_1 - int_a)*(arr_len + 1)/(int_b-int_a)));
texty = texty_1 + 0.075;
plot([textx_1, textx], [texty_1, texty], 'b--', 'LineWidth', 1.75);
text(textx, texty, ...
    '$\,$Chernoff approximation via (15)', ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'right', 'Interpreter', 'latex');

% 2nd approximation
plot(x, chern_iter_2, '-r', 'LineWidth', 1.75)
textx_1 = 0.032;
textx = textx_1 + 0.725;
texty_1 = chern_iter_2(floor((textx_1 - int_a)*(arr_len + 1)/(int_b-int_a)));
texty = texty_1 + 0.075;
plot([textx_1, textx], [texty_1, texty], 'r--', 'LineWidth', 1.75);
text(textx, texty, ...
    '$\,$Chernoff approximation via (16)', ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

hold off