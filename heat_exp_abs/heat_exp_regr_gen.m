clear

% variables declaration
t = 1; a = 1; N = 100;
int_a = -50; int_b = 50;
arr_len = 1001;
x = linspace(int_a, int_b, arr_len);
n_lin = linspace(6, N, arr_len);
norm_1 = zeros(1, N);
norm_2 = zeros(1, N);
norm_3 = zeros(1, N);

% auxiliary functions
y = linspace(1, N+1, arr_len);
ln_1 = @(x)(-x);
ln_2 = @(x)(-2*x);
inv = @(x)(1./x);
inv_2 = @(x)(1./(x.^2));

% initial condition and solution
init = @(x)(exp(-abs(x)));
solution = @(x)(exp(t-x).*(1 - 1/2*erfc(x/(2*sqrt(t)) - sqrt(t))) + ...
       exp(t+x)*1/2.*erfc(x/(2*sqrt(t)) + sqrt(t)));

% iteration on composition degree
for n = 1:N
    
    % shift components
    tau_R = 2*a*sqrt(t/n);
    tau_V = a*sqrt(6*t/n);    
    
    % computing Chernoff functions
    for p = 0:n
        
        % declaring coefficients
        alpha_R = 0;
        alpha_V = 0;
        
        % computing coefficients
        for k = 0:floor((n - abs(p))/2)
            alpha_R = alpha_R + nchoosek(n, k)*nchoosek(n-k, k + abs(p))*2^(n - abs(p) - 2*k);
            alpha_V = alpha_V + nchoosek(n, k)*nchoosek(n-k, k + abs(p))*4^(n - abs(p) - 2*k);
        end
        
        % computing Chernoff
        if (p == 0)
            chern_R = alpha_R*init(x);
            chern_V = alpha_V*init(x);
        else
            chern_R = chern_R + alpha_R*(init(x + p*tau_R) + init(x - p*tau_R));
            chern_V = chern_V + alpha_V*(init(x + p*tau_V) + init(x - p*tau_V));
        end
    end
    
    % computing norms
    
    % Remizov function
    chern_R = 4^(-n)*chern_R;
    norm_1(n) = max(abs(chern_R - solution(x)));

    % Vedenin's function
    chern_V = 6^(-n)*chern_V;
    norm_2(n) = max(abs(chern_V - solution(x)));
end

% computing linear regression
regr_beta_1 = fitlm(log(6:N), log(norm_1(6:N)), 'linear').Coefficients.Estimate(1);
regr_alpha_1 = fitlm(log(6:N), log(norm_1(6:N)), 'linear').Coefficients.Estimate(2);
regr_beta_2 = fitlm(log(6:N), log(norm_2(6:N)), 'linear').Coefficients.Estimate(1);
regr_alpha_2 = fitlm(log(6:N), log(norm_2(6:N)), 'linear').Coefficients.Estimate(2);

regr_1 = exp(regr_beta_1).*n_lin.^regr_alpha_1;
regr_2 = exp(regr_beta_2).*n_lin.^regr_alpha_2;

% plotting graphs
figure
hold on

% axes names and limits
xlabel('n', 'FontSize', 18);
ylabel('u', 'FontSize', 18);
ax = gca;
set(gca, 'FontSize', 18);
ax.XAxisLocation = 'origin';
% ax.YAxisLocation = 'origin';
xlim([4.5 N]);
y_lim = 0.05;
ylim([.0 y_lim]);

% title
textx = (100 + 4.5)/2;
% text(textx, y_lim - 0.004, ...
%     ['Estimates of the convergence speed of ', ...
%     'Chernoff approximations to the solution'], ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');
% text(textx, y_lim - 0.008, ...
%     ['of the heat equation $u^\prime_t = a\cdot u^{\prime\prime}_{xx}$', ...
%     ' for $u_0(x) = \exp(-|x|)$, $a = 1$, and $t = 1$'], ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');

% 1st approximation
plot(6:N, norm_1(6:N), 'o', 'MarkerSize', 8, ...
    'MarkerEdgeColor', 'b', 'MarkerFaceColor', 'b');
plot(n_lin, regr_1, 'k-', 'LineWidth', 1.75, 'HandleVisibility', 'off');
textx_1 = 13.5;
textx = textx_1 + 10;
texty_1 = regr_1(floor((textx_1 - 6)*(arr_len + 1)/(N-6)));
texty = texty_1 + 0.019;
plot([textx_1, textx], [texty_1, texty], 'k--', 'LineWidth', 1.75, ...
    'HandleVisibility', 'off');
text(textx, texty, ...
    ['$\, \beta_1\left(\frac{1}{n}\right)^{\alpha_1}$ with $\beta_1 = ', ...
    num2str(regr_beta_1, '%3.4f'), '$, $\alpha_1 = ', ...
    num2str(regr_alpha_1, '%3.4f'), '$'], ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% 2nd approximation
plot(6:N, norm_2(6:N), 'o', 'MarkerSize', 8, ...
    'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r');
plot(n_lin, regr_2, 'k-', 'LineWidth', 1.75, 'HandleVisibility', 'off');
textx_1 = 11.5;
textx = textx_1 + 9;
texty_1 = regr_2(floor((textx_1 - 6)*(arr_len + 1)/(N-6)));
texty = texty_1 + 0.0165;
plot([textx_1, textx], [texty_1, texty], 'k--', 'LineWidth', 1.75, ...
    'HandleVisibility', 'off');
text(textx, texty, ...
    ['$\, \beta_2\left(\frac{1}{n}\right)^{\alpha_2}$ with $\beta_2 = ', ...
    num2str(regr_beta_2, '%3.4f'), '$, $\alpha_2 = ', ...
    num2str(regr_alpha_2, '%3.4f'), '$'], ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% legend
lgd = legend('Norm decay for Chernoff approximation via (15)', ...
    'Norm decay for Chernoff approximation via (16)', ...
    'Interpreter', 'latex', 'FontSize', 26);
rect = [0.575, 0.25, .225, .25];
set(lgd, 'Position', rect)

hold off