clear

% variables declaration
t = 2; a = 1;
int_a = -30; int_b = 10;
arr_len = 1001;
x = linspace(int_a, int_b, arr_len);
N = 100;
norm_1 = zeros(1, N);
norm_2 = zeros(1, N);
norm_3 = zeros(1, N);

% auxiliary functions
y = linspace(1, N+1, arr_len);
ln_1 = @(x)(-x);
ln_2 = @(x)(-2*x);
inv = @(x)(1./x);
inv_2 = @(x)(1./(x.^2));

% initial condition and solution
init = @(x)(exp(-abs(x)));
solution = @(x)(exp(-abs(x + t)));

% iteration on composition degree
for n = 1:N
    
    % computing approximations
    chern_1 = init(x + t + a*t^2/n);
    chern_2 = init(x + t + a*t^3/n^2);
    chern_3 = init(x + t + a*t^4/n^3);
        
    % computing norms
    norm_1(n) = max(abs(chern_1 - solution(x)));
    norm_2(n) = max(abs(chern_2 - solution(x)));
    norm_3(n) = max(abs(chern_3 - solution(x)));

    if (n == 1)
        chern_iter_1 = chern_1;
        chern_iter_2 = chern_2;
        chern_iter_3 = chern_3;
    end
end

% plotting graphs
figure
hold on

% axes names and limits
xlabel('n', 'FontSize', 16);
xlim([4.5 N + 1]);
y_lim = 0.8;
ylim([-0.05 y_lim]);

% title
textx = (N + 5.5)/2;
text(textx, y_lim - 0.05, ...
    'Convergence speed of Chernoff approximations to the solution of the transport equation', ...
    'Interpreter', 'latex', 'FontSize', 24, ...
    'HorizontalAlignment', 'center');
text(textx, y_lim - 0.1, ...
    ['$u^\prime_t = u^{\prime}_{x}$ for $u_0(x) = \exp(-|x|)$,', ...
    ' $a = 1$, and $t = 2$'], ...
    'Interpreter', 'latex', 'FontSize', 24, ...
    'HorizontalAlignment', 'center');

% 1st approximation
plot(1:N, norm_1, 'o', 'MarkerSize', 8, ...
    'MarkerEdgeColor', 'b', 'MarkerFaceColor', 'b');
textx_1 = 25;
textx = textx_1 + 5.5;
texty_1 = norm_1(textx_1);
texty = texty_1 + 0.175;
plot([textx_1, textx], [texty_1, texty], 'b--', 'LineWidth', 1.5, ...
    'HandleVisibility', 'off');
text(textx, texty, ...
    '$\,$Norm decay for Chernoff approximation via (8), $k = 1$', ...
    'FontSize', 22, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% 2nd approximation
plot(1:N, norm_2, 'o', 'MarkerSize', 8, ...
    'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r');
textx_1 = 14;
textx = textx_1 + 12;
texty_1 = norm_2(textx_1);
texty = texty_1 + 0.4;
plot([textx_1, textx], [texty_1, texty], 'r--', 'LineWidth', 1.5, ...
    'HandleVisibility', 'off');
text(textx, texty, ...
    '$\,$Norm decay for Chernoff approximation via (8), $k = 2$', ...
    'FontSize', 22, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% 3rd approximation
plot(1:N, norm_3, 'o', 'MarkerSize', 8, ...
    'MarkerEdgeColor', '#77AC30', 'MarkerFaceColor', '#77AC30');
textx_1 = 7;
textx = textx_1 + 15;
texty_1 = norm_3(textx_1);
texty = texty_1 + 0.5;
plot([textx_1, textx], [texty_1, texty], 'LineStyle', '--', ...
    'Color', '#77AC30', 'LineWidth', 1.5, 'HandleVisibility', 'off');
text(textx, texty, ...
    '$\,$Norm decay for Chernoff approximation via (8), $k = 3$', ...
    'FontSize', 22, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

hold off