clear

% variables declaration
t = 2; a = 1;
int_a = -5; int_b = 3;
arr_len = 1001;
x = linspace(int_a, int_b, arr_len);
N = 100;
norm_1 = zeros(1, N);
norm_2 = zeros(1, N);
norm_3 = zeros(1, N);

% auxiliary functions
y = linspace(1, N+1, arr_len);
ln_1 = @(x)(-x);
ln_2 = @(x)(-2*x);
inv = @(x)(1./x);
inv_2 = @(x)(1./(x.^2));

% initial condition and solution
init = @(x)(exp(-abs(x)));
solution = @(x)(exp(-abs(x + t)));

% iteration on composition degree
for n = 1:N
    
    % computing approximations
    chern_1 = init(x + t + a*t^2/n);
    chern_2 = init(x + t + a*t^3/n^2);
    chern_3 = init(x + t + a*t^4/n^3);
        
    % computing norms
    norm_1(n) = max(abs(chern_1 - solution(x)));
    norm_2(n) = max(abs(chern_2 - solution(x)));
    norm_3(n) = max(abs(chern_3 - solution(x)));

    if (n == 5)
        chern_iter_1 = chern_1;
        chern_iter_2 = chern_2;
        chern_iter_3 = chern_3;
    end
end

% plotting graphs
figure
hold on

% axes names and limits
xlabel('x', 'FontSize', 18);
ylabel('u', 'FontSize', 18);
ax = gca;
set(gca, 'FontSize', 18);
ax.XAxisLocation = 'origin';
% ax.YAxisLocation = 'origin';
xlim([int_a int_b]);
y_lim = 1.05;
ylim([0 y_lim]);

% title
textx = (int_b + int_a)/2;
% text(textx, y_lim - 0.075, ...
%     ['Approximations to the solution of the transport equation ', ...
%     '$u^\prime_t = u^{\prime}_{x}$ ', ...
%     'for $u_0(x) = \exp(-|x|)$, '], ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');
% text(textx, y_lim - 0.15, ...
%     '$a = 1$, and $t = 2$, using Chernoff functions (n = 5)', ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');
        
% solution
plot(x, solution(x), '-k', 'LineWidth', 1.75); 
textx_1 = -0.75;
texty_1 = solution(textx_1);
textx = textx_1 + 0.6;
texty = texty_1 + 0.175;
text(textx, texty, '$\,$Exact solution $u(t, x) = \exp(-|x + t|)$', ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');
plot([textx_1, textx], [texty_1, texty], ...
    'k--', 'LineWidth', 1.75);
        
% 1st approximation
plot(x, chern_iter_1, '-b', 'LineWidth', 1.75);
textx_1 = -2.325;
textx = textx_1 + 1;
texty_1 = chern_iter_1(floor((textx_1 - int_a)*(arr_len + 1)/(int_b-int_a)));
texty = texty_1 + 0.295;
plot([textx_1, textx], [texty_1, texty], 'b--', 'LineWidth', 1.75);
text(textx, texty, ...
    '$\,$Chernoff approximation via (8), $k = 1$', ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% 2nd approximation
plot(x, chern_iter_2, '-r', 'LineWidth', 1.75)
textx_1 = -1.7;
textx = textx_1 + 0.75;
texty_1 = chern_iter_2(floor((textx_1 - int_a)*(arr_len + 1)/(int_b-int_a)));
texty = texty_1 + 0.225;
plot([textx_1, textx], [texty_1, texty], 'r--', 'LineWidth', 1.75);
text(textx, texty, ...
    '$\,$Chernoff approximation via (8), $k = 2$', ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% 3rd approximation
plot(x, chern_iter_3, 'LineStyle', '-', 'Color', '#77AC30', 'LineWidth', 1.75)
textx_1 = -1.225;
textx = textx_1 + 0.7;
texty_1 = chern_iter_3(floor((textx_1 - int_a)*(arr_len + 1)/(int_b-int_a)));
texty = texty_1 + 0.21;
plot([textx_1, textx], [texty_1, texty], 'LineStyle', '--', ...
    'Color', '#77AC30', 'LineWidth', 1.75);
text(textx, texty, ...
    '$\,$Chernoff approximation via (8), $k = 3$', ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

hold off