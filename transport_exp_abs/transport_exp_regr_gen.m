clear

% variables declaration
t = 1; a = 1; N = 100;
int_a = -50; int_b = 50;
arr_len = 5001;
x = linspace(int_a, int_b, arr_len);
n_lin = linspace(6, N, arr_len);
norm_1 = zeros(1, N);
norm_2 = zeros(1, N);
norm_3 = zeros(1, N);

% auxiliary functions
y = linspace(1, N+1, arr_len);
ln_1 = @(x)(-x);
ln_2 = @(x)(-2*x);
inv = @(x)(1./x);
inv_2 = @(x)(1./(x.^2));

% initial condition and solution
init = @(x)(exp(-abs(x)));
solution = @(x)(exp(-abs(x + t)));

% iteration on composition degree
for n = 1:N
    
    % computing approximations
    chern_1 = init(x + t + a*t^2/n);
    chern_2 = init(x + t + a*t^3/n^2);
    chern_3 = init(x + t + a*t^4/n^3);
        
    % computing norms
    norm_1(n) = max(abs(chern_1 - solution(x)));
    norm_2(n) = max(abs(chern_2 - solution(x)));
    norm_3(n) = max(abs(chern_3 - solution(x)));

    if (n == 1)
        chern_iter_1 = chern_1;
        chern_iter_2 = chern_2;
        chern_iter_3 = chern_3;
    end
end

% computing linear regression
regr_beta_1 = fitlm(log(6:N), log(norm_1(6:N)), 'linear').Coefficients.Estimate(1);
regr_alpha_1 = fitlm(log(6:N), log(norm_1(6:N)), 'linear').Coefficients.Estimate(2);
regr_beta_2 = fitlm(log(6:N), log(norm_2(6:N)), 'linear').Coefficients.Estimate(1);
regr_alpha_2 = fitlm(log(6:N), log(norm_2(6:N)), 'linear').Coefficients.Estimate(2);
regr_beta_3 = fitlm(log(6:N), log(norm_3(6:N)), 'linear').Coefficients.Estimate(1);
regr_alpha_3 = fitlm(log(6:N), log(norm_3(6:N)), 'linear').Coefficients.Estimate(2);

regr_1 = exp(regr_beta_1).*n_lin.^regr_alpha_1;
regr_2 = exp(regr_beta_2).*n_lin.^regr_alpha_2;
regr_3 = exp(regr_beta_3).*n_lin.^regr_alpha_3;

% plotting graphs
figure
hold on

% axes names and limits
xlabel('n', 'FontSize', 18);
ylabel('u', 'FontSize', 18);
ax = gca;
set(gca, 'FontSize', 18);
ax.XAxisLocation = 'origin';
% ax.YAxisLocation = 'origin';
xlim([4.5 100]);
y_lim = 0.165;
ylim([0 y_lim]);

% title
textx = (100 + 4.5)/2;
% text(textx, y_lim - 0.0175, ...
%     'Estimates of the convergence speed of Chernoff approximations to the solution', ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');
% text(textx, y_lim - 0.035, ...
%     ['of the transport equation $u^\prime_t = u^{\prime}_{x}$', ...
%     ' for $u_0(x) = \exp(-|x|)$, $a = 1$, and $t = 1$'], ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');

% 1st approximation
plot(6:N, norm_1(6:N), 'o', 'MarkerSize', 8, ...
    'MarkerEdgeColor', 'b', 'MarkerFaceColor', 'b');
plot(n_lin, regr_1, 'k-', 'LineWidth', 1.75, 'HandleVisibility', 'off');
textx_1 = 24.5;
textx = textx_1 + 4;
texty_1 = regr_1(floor((textx_1 - 6)*(arr_len + 1)/(N-6)));
texty = texty_1 + 0.035;
plot([textx_1, textx], [texty_1, texty], 'k--', 'LineWidth', 1.75, ...
    'HandleVisibility', 'off');
text(textx, texty, ...
    ['$\, \beta_1\left(\frac{1}{n}\right)^{\alpha_1}$ with $\beta_1 = ', ...
    num2str(regr_beta_1, '%3.4f'), '$, $\alpha_1 = ', ...
    num2str(regr_alpha_1, '%3.4f'), '$'], ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% 2nd approximation
plot(6:N, norm_2(6:N), 'o', 'MarkerSize', 8, ...
    'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r');
plot(n_lin, regr_2, 'k-', 'LineWidth', 1.75, 'HandleVisibility', 'off');
textx_1 = 13.52;
textx = textx_1 + 11.5;
texty_1 = regr_2(floor((textx_1 - 6)*(arr_len + 1)/(N-6)));
texty = texty_1 + 0.095;
plot([textx_1, textx], [texty_1, texty], 'k--', 'LineWidth', 1.75, ...
    'HandleVisibility', 'off');
text(textx, texty, ...
    ['$\, \beta_2\left(\frac{1}{n}\right)^{\alpha_2}$ with $\beta_2 = ', ...
    num2str(regr_beta_2, '%3.4f'), '$, $\alpha_2 = ', ...
    num2str(regr_alpha_2, '%3.4f'), '$'], ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% 3rd approximation
plot(6:N, norm_3(6:N), 'o', 'MarkerSize', 8, ...
    'MarkerEdgeColor', '#77AC30', 'MarkerFaceColor', '#77AC30');
plot(n_lin, regr_3, 'k-', 'LineWidth', 1.75, 'HandleVisibility', 'off');
textx_1 = 6.5;
textx = textx_1 + 15;
texty_1 = regr_3(floor((textx_1 - 6)*(arr_len + 1)/(N-6)));
texty = texty_1 + 0.125;
plot([textx_1, textx], [texty_1, texty], '--', 'Color', 'k', ...
    'LineWidth', 1.75, 'HandleVisibility', 'off');
text(textx, texty, ...
    ['$\, \beta_3\left(\frac{1}{n}\right)^{\alpha_3}$ with $\beta_3 = ', ...
    num2str(regr_beta_3, '%3.4f'), '$, $\alpha_3 = ', ...
    num2str(regr_alpha_3, '%3.4f'), '$'], ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');

% legend
lgd = legend('Norm decay for Chernoff approximation via (8), $k = 1$', ...
    'Norm decay for Chernoff approximation via (8), $k = 2$', ...
    'Norm decay for Chernoff approximation via (8), $k = 3$', ...
    'Interpreter', 'latex', 'FontSize', 26);
rect = [0.575, 0.215, .15, .25];
set(lgd, 'Position', rect)

hold off