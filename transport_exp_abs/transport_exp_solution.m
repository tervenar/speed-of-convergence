clear

% variables declaration
t = 1; a = 1; N = 100;
int_a = -11; int_b = 10;
arr_len = 1001;
x = linspace(int_a, int_b, arr_len);
norm_1 = zeros(1, N);
norm_2 = zeros(1, N);
norm_3 = zeros(1, N);

% auxiliary functions
y = linspace(1, N+1, arr_len);
ln_1 = @(x)(-x);
ln_2 = @(x)(-2*x);
inv = @(x)(1./x);
inv_2 = @(x)(1./(x.^2));

% initial condition and solution
init = @(x)(exp(-abs(x)));
solution = @(x)(exp(-abs(x + t)));

% plotting graphs
figure
hold on

% axes names and limits
xlabel('x', 'FontSize', 18);
ylabel('u', 'FontSize', 18);
ax = gca;
set(gca, 'FontSize', 18);
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
xlim([int_a int_b]);
y_lim = 1.1;
ylim([0 y_lim]);

% title
textx = (int_b + int_a)/2;
% text(textx, y_lim - 0.075, ...
%     ['Solution of the transport equation ', ...
%     '$u^{\prime}_t = u^{\prime}_{x}$ ', ...
%     'for $u_0(x) = \exp(-|x|)$ and $t = 1$'], ...
%     'Interpreter', 'latex', 'FontSize', 24, ...
%     'HorizontalAlignment', 'center');
        
% initial condition
plot(x, init(x), '-', 'Color', '#D95319', 'LineWidth', 1.75); 
textx_1 = 0.5;
texty_1 = init(textx_1);
textx = textx_1 + 1;
texty = texty_1 + 0.15;
text(textx, texty, '$\,$Initial condition $u_0(x)$', ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'left', 'Interpreter', 'latex');
plot([textx_1, textx], [texty_1, texty], ...
    '--', 'Color', '#D95319', 'LineWidth', 1.75);
        
% solution
plot(x, solution(x), '-k', 'LineWidth', 1.75); 
textx_1 = -1.5;
texty_1 = solution(textx_1);
textx = textx_1 - 1;
texty = texty_1 + 0.15;
text(textx, texty, '$\,$Exact solution $u(t, x) = \exp(-|x + t|)$', ...
    'FontSize', 26, 'HorizontalAlignment', ...
    'right', 'Interpreter', 'latex');
plot([textx_1, textx], [texty_1, texty], ...
    'k--', 'LineWidth', 1.75);

hold off